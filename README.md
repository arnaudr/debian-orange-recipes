# Debian Orange Recipes

This recipe create a minimal Debian image, creates an unprivileged user,
installs and enables the SSH server. Wired network is brought up with
systemd-networkd. Wireless network is not supported.

Tested with Orange Pi Zero LTS.

Note that the package `u-boot-sunxi` provides `/usr/lib/u-boot/orangepi_plus`
and `/usr/lib/u-boot/orangepi_zero`, so it probably works with more boards.

Install `firmware-misc-nonfree` if you want to avoid this:

```
update-initramfs: Generating /boot/initrd.img-5.17.0-3-armmp
W: Possible missing firmware /lib/firmware/imx/sdma/sdma-imx6q.bin for built-in driver imx_sdma
```

Build for architecture `arm64` didn't work when I tried.
