#!/bin/bash

set -eu

IMAGE=    # $1
TARGET=   # $2
GROW=1    # -g

USAGE="Usage: $(basename $0) [-g] IMAGE TARGET"

warn() { echo "WARNING:" "$@" >&2; }
fail() { echo "ERROR:" "$@" >&2; exit 1; }
bold() { tput bold; echo "> $@"; tput sgr0; }

assert_block_device() {
    [ -b "$1" ] || fail "'$1' is not a block device."
}

assert_exists() {
    [ -e "$1" ] || fail "'$1' does not exist."
}

assert_commands() {
    local cmd=

    while (( $# )); do
        cmd=$1 && shift
        # commands in PATH
        command -v $cmd >/dev/null 2>&1 && continue
        # commands that might not be in PATH
        [ -x /sbin/$cmd -o -x /usr/sbin/$cmd ] && continue
        # commands really not found
        fail "Command '$cmd' not found, please install it."
    done
}

confirm() {
    local question=${1:-"Do you want to continue?"}
    local answer=

    while read -r -t 0; do read -n 256 -r -s; done
    read -r -p "$question [Y/n] " answer
    [ "$answer" ] && answer=${answer,,} || answer=y
    case "$answer" in
        (y|yes) return 0 ;;
        (*)     return 1 ;;
    esac
}

validate_target() {
    local target=$1
    local devtype=
    local removable=

    assert_block_device "$target"

    devtype=$(lsblk --raw --nodeps -no type "$target")
    if [ "$devtype" != "disk" ]; then
        warn "'$target' is not a disk (type=$devtype)!"
    fi

    removable=$(lsblk --raw --nodeps -no rm "$target")
    if [ "$removable" -eq 0 ]; then
        warn "'$target' is not a removable device!"
    fi

    echo "All data on '$target' will be lost! No way back!"
    confirm || exit 1
}

prepare_target() {
    local target=$1
    local dev=

    assert_block_device "$target"

    bold "Unmounting everything from '$target'"
    for dev in $(findmnt -o source | grep "$target" | sort); do
        udisksctl unmount -b "$dev"
    done
}

write_image() {
    local image=$1
    local target=$2

    assert_exists "$image"
    assert_block_device "$target"

    bold "Copying image '$image' to '$target', this might take a while..."
    sudo cp "$image" "$target"
    sync
}

finalize_target() {
    local target=$1
    local grow=$2

    assert_block_device "$target"

    bold "Fixing GPT secondary header"
    echo fix | sudo parted ---pretend-input-tty "$target" print

    bold "Telling the kernel about partition table changes"
    sudo partprobe -s "$target"
    sudo udevadm trigger
    sudo udevadm settle

    if (( grow )); then
        local parted_last_line=
        local last=
        local lastdev=
        local lastname=

        parted_last_line=$(sudo parted -m "$target" print | tail -1)
        last=$(echo "$parted_last_line" | cut -d':' -f1)
        lastdev=${target}${last}
        [ -b "$lastdev" ] || lastdev=${target}p${last}

        if [ -b "$lastdev" ]; then
            lastname=$(lsblk -n -o label "$lastdev")

            bold "Growing the last partition $lastdev ($lastname)"
            sudo parted "$target" resizepart "$last" 100%

            bold "Telling the kernel about partition table changes"
            sudo partprobe -s "$target"
            sudo udevadm trigger
            sudo udevadm settle

            bold "Resizing the filesystem on $lastdev"
            sudo e2fsck -f "$lastdev"
            sudo resize2fs "$lastdev"
        else
            warn "Failed to locate the last partition on $target."
            warn "Can't grow the last partition to fill the device."
        fi
    fi

    bold "Powering off $target"
    udisksctl power-off -b "$target"
}

# main

while getopts ":fgh" opt; do
    case $opt in
        (g) GROW=0 ;;
        (h) echo "$USAGE"; exit 0 ;;
        (*) echo "$USAGE" >&2; exit 1 ;;
    esac
done
shift $((OPTIND - 1))

[ $# -eq 2 ] || fail "$USAGE"
IMAGE=$1 && TARGET=$2 && shift 2

assert_commands partprobe parted resize2fs sudo udisksctl
validate_target "$TARGET"
prepare_target "$TARGET"
write_image "$IMAGE" "$TARGET"
finalize_target "$TARGET" "$GROW"

echo "Done!"
