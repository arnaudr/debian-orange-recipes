#!/bin/bash

set -eu

debos -t nonfree:true -t username:$USER -t userpass:$USER "$@"
